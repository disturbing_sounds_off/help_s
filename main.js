
let chislo_1 = document.getElementById("chislo1") 
let chislo_2 = document.getElementById("chislo2") 
let rez = document.getElementById("rez") 
let istoria = document.getElementById("istoria") 

function summa(){ 
    let sum = Number(chislo_1.value) + Number(chislo_2.value) 
    rez.innerHTML = sum 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_1.value} + ${chislo_2.value} = ${sum}` 
    istoria.appendChild(history_ellement) 
} 


function minus(){ 
    let min = Number(chislo_1.value) - Number(chislo_2.value) 
    rez.innerHTML = min 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_1.value} - ${chislo_2.value} = ${min}` 
    istoria.appendChild(history_ellement) 
} 


function delenie(){ 
    let delen = Number(chislo_1.value) / Number(chislo_2.value) 
    rez.innerHTML = delen 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_1.value} / ${chislo_2.value} = ${delen}` 
    istoria.appendChild(history_ellement) 
} 



function ymnozenie(){ 
    let ymnoz = Number(chislo_1.value) * Number(chislo_2.value) 
    rez.innerHTML = ymnoz 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_1.value} * ${chislo_2.value} = ${ymnoz}` 
    istoria.appendChild(history_ellement) 
} 


function koren(){ 
    let kor = Math.pow(Number(chislo_1.value), 1/Number(chislo_2.value)) 
    rez.innerHTML = kor 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_2.value} √ ${chislo_1.value} = ${kor}` 
    istoria.appendChild(history_ellement) 
} 


function prosent(){ 
    let pros = Number(chislo_1.value) * 0.01* Number(chislo_2.value) 
    rez.innerHTML = pros 

    let history_ellement = document.createElement("li") 

    history_ellement.innerHTML = `${chislo_1.value} % ${chislo_2.value} = ${pros}` 
    istoria.appendChild(history_ellement) 
} 






